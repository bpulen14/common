/*
 * FileSystemOperations.cpp
 *
 *  Created on: 1-10-2012
 *      Author: CS
 */

#include "ros/package.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"
#include "boost/progress.hpp"
#include <sys/timeb.h>
#include <iomanip>      // std::setfill, std::setw
#include <boost/filesystem.hpp>
#include <sys/stat.h>

#include "FileSystemOperations.hpp"

namespace fs = boost::filesystem;
using namespace std;

FileSystemOperations::FileSystemOperations()
{

}

FileSystemOperations::~FileSystemOperations()
{

}

string FileSystemOperations::getCurrentPath(void)
{
    boost::filesystem::path full_path( boost::filesystem::current_path() );
    return full_path.string();
}

void FileSystemOperations::getPath(string mobile_manipulator_loc, char* out_path)
{
	/*
	* Returns the full path to the location on the current machine
	* REMEMBER to free the char*
	*
	* 'location' must be set to the location a ROS package and subdir,
	* example: location = "/Data/Calibrations/Cameras/";
	*/

//    //get the path to Skill_Based_System:
//    string path = ros::package::getPath("Skill_Based_System");

//    //Chop down to path to "mobile_manipulator_loc" and add the supplied location
//    size_t found;
//    string key = "mobile_manipulator_loc";

//    found=path.rfind(key);
//	 if (found!=string::npos)
//	 {
//		 path.replace(path.begin()+found+key.size(), path.end(), mobile_manipulator_loc);
//	 }

	string path = getPath(mobile_manipulator_loc);

	//Create an array of chars containing the filename to be used when using ofstream
	int size = path.size();
	for (int a=0;a<=size;a++)
	{
	  out_path[a]=path[a];
	}

}

string FileSystemOperations::getPath(string mobile_manipulator_loc)
{
	/*
	* Returns the full path to the location on the current machine
	*
	* 'location' must be set to the location a ROS package and subdir,
	* example: location = "/Data/Calibrations/Cameras/";
	*/

	// Determine package:
	size_t start = mobile_manipulator_loc.find_first_of("/")+1;
	size_t stop  = mobile_manipulator_loc.find_first_of("/", start);
	if (start == string::npos || stop == string::npos)
	{
		cerr << "Attempting to load invalid path: '" << mobile_manipulator_loc << "'" << endl;
		return "";
	}

	// Reading package name:
	string package = mobile_manipulator_loc.substr(start, stop-start);

	// If legacy "Data" is used, change it:
	if (package == "Data")
		package = "sbs_data";


	string rest = mobile_manipulator_loc.substr(stop);

	//get the path to the package (Skill_Based_System or Data):
	string path = ros::package::getPath(package) + rest;

	return path;
}

string FileSystemOperations::getAnyPath(string search_key, string path_from_key, bool replace_key = false)
{
    /*
     * Searches the current directory (that is, the directory of the current executable),
     * chops down to the search_key (deletes the search key if replace_key = true) and then adds the path_from_key.
     */

    //get the full path of the executable - No longer possible after migration to catkin. Instead get full path to Skill_Based_System
//    char buff[1024];
//    ssize_t len = ::readlink("/proc/self/exe", buff, sizeof(buff)-1);
//    string path = string(buff);

    //get the path to Skill_Based_System:
    string path = ros::package::getPath("Skill_Based_System");

    //Chop down to path to search key and add the supplied location
    //one could discuss whether to search from the beginning (find) or the end (rfind).
    //ideally, it should not matter, as the key should be unique. However, since this is a
    //library used in many places, we cannot guarantee how it is used. This one uses "find" for now!

    size_t found=path.find(search_key);

    if (found!=string::npos)
    {
        if(replace_key) path.replace (path.begin()+found, path.end(),path_from_key);
        else path.replace (path.begin()+found+search_key.size(), path.end(),path_from_key);
    }
    else
    {
        cout << "Error - current path doesn't contain: " << search_key  << endl;
        return "error";
    }

    return path;
}

void FileSystemOperations::createDir(std::string dir)
{
    boost::filesystem::create_directories(dir);
}

bool FileSystemOperations::getFileList(char path[200], vector<string>& file_list)
{
    /*
     * This function reads all the files in a given folder.
     */

    fs::path p(fs::current_path());

    p = fs::system_complete(path);

    unsigned long file_count = 0;
    unsigned long dir_count = 0;
    unsigned long other_count = 0;
    unsigned long err_count = 0;

    if (!fs::exists(p))
    {
        std::cerr << "\nNot found: " << p << std::endl;
        return false;
    }

	if (!fs::is_directory(p))
	{
		// must be a file
		std::cerr << "\nFound: " << p << "\n";
		return false;
	}

	//		std::cout << "\nIn directory: " << p << "\n\n";
	fs::directory_iterator end_iter;
	for (fs::directory_iterator dir_itr(p); dir_itr != end_iter; ++dir_itr)
	{
		try
		{
			if (fs::is_regular_file(dir_itr->status()))
			{
				string filename = dir_itr->path().filename().string();
				if (filename.rfind("~") == string::npos)
				{
					++file_count;
					//						std::cout << dir_itr->path().filename() << "\n";
					file_list.push_back(filename);
				}
			}

		}
		catch (const std::exception & ex)
		{
			++err_count;
			//				std::cout << dir_itr->path().filename() << " " << ex.what() << std::endl;
		}
	}
	//		std::cout << "\n" << file_count << " files";

    return true;

}

bool FileSystemOperations::getFileList(string path_string, vector<string>& file_list)
{
    /*
     * This function reads all the files in a given folder.
     */

    fs::path p(fs::current_path());
    p = fs::system_complete(path_string.c_str());
    unsigned long file_count = 0;
	vector<FileTime> files;

    if (!fs::exists(p))
    {
        std::cerr << "\nNot found: " << p << std::endl;
        return false;
    }

	if (!fs::is_directory(p))
	{
		// must be a file
		std::cerr << "\nFound: " << p << "\n";
		return false;
	}
//			std::cout << "\nIn directory: " << p << "\n\n";
	fs::directory_iterator end_iter;
	for (fs::directory_iterator dir_itr(p);dir_itr != end_iter;++dir_itr)
	{
		try
		{
			if (fs::is_regular_file(dir_itr->status()))
			{
				string filename = dir_itr->path().filename().string();
				if (filename.rfind("~") == string::npos)
				{
					++file_count;
//											std::cout << dir_itr->path().filename() << "\n";
					files.push_back(FileTime(filename, fs::last_write_time(dir_itr->path())));
				}
			}

		}
		catch (const std::exception & ex)
		{
			std::cout << dir_itr->path().filename() << " " << ex.what() << std::endl;
			return false;
		}
	}
	//		std::cout << "\n" << file_count << " files\n";

	std::sort(files.begin(), files.end(), by_time());

	for (int i = 0; i < (int)files.size(); i++)
	{
		file_list.push_back(files[i].filename_);
	}

    return true;

}

bool FileSystemOperations::getFileList(string path_string, vector<string>& file_list, string containing)
{
    if (!getFileList(path_string, file_list))
        return false;

    // Remove unrelevant files:
    for (int i = (int)file_list.size()-1; i >= 0; i--)
    {
        if(file_list[i].find(containing) == string::npos)
        {
            file_list.erase(file_list.begin()+i);
        }
    }

    return true;
}

bool FileSystemOperations::getFolderList(string path_string, vector<string>& folder_list)
{
    /*
     * This function reads all the files in a given folder.
     */

    fs::path p(fs::current_path());

    p = fs::system_complete(path_string.c_str());

    unsigned long dir_count = 0;

    if (!fs::exists(p))
    {
        std::cout << "\nNot found: " << p << std::endl;
        return false;
    }

    if (fs::is_directory(p))
    {
        fs::directory_iterator end_iter;
        for (fs::directory_iterator dir_itr(p);dir_itr != end_iter;++dir_itr)
        {
            try
            {
                if (fs::is_directory(dir_itr->status()))
                {
                    size_t found = dir_itr->path().filename().string().rfind("."); //path.rfind(key);

                    if (found == string::npos)
                    {
                        ++dir_count;
                        folder_list.push_back(dir_itr->path().filename().string());
                    }
                }
            }
            catch (const std::exception & ex)
            {
                std::cout << dir_itr->path().filename() << " " << ex.what() << std::endl;
            }
        }


    }
    else // must be a file
    {
        //std::cout << "\nFound: " << p << "\n";
    }

    return true;

}

bool FileSystemOperations::getFolderList(string path_string, vector<string>& folder_list, string containing)
{
    if (!getFolderList(path_string, folder_list))
        return false;

    // Remove unrelevant folders:
    for (int i = (int)folder_list.size()-1; i >= 0; i--)
    {
        if(folder_list[i].find(containing) == string::npos)
        {
            folder_list.erase(folder_list.begin()+i);
        }
    }

    return true;
}

bool FileSystemOperations::deleteFile(string path_string, string filename)
{
    fs::path p(fs::current_path());

    p = fs::system_complete(path_string.c_str());

    if (!fs::exists(p))
    {
        std::cout << "\nNot found: " << p << std::endl;
        return false;
    }

    if (fs::is_directory(p))
    {
        fs::directory_iterator end_iter;
        for (fs::directory_iterator dir_itr(p);dir_itr != end_iter;++dir_itr)
        {
            try
            {
                if (fs::is_regular_file(dir_itr->status()))
                {
                    if(dir_itr->path().filename().string() == filename)
                    {
                        fs::remove(*dir_itr);
                        return true;
                    }
                }

            }
            catch (const std::exception & ex)
            {
                std::cout << dir_itr->path().filename() << " " << ex.what() << std::endl;
            }
        }

    }

    return false;
}

bool FileSystemOperations::deleteFile(string filename)
{
    struct stat st;
    lstat(filename.c_str(), &st);
    if(S_ISDIR(st.st_mode))
    {
        std::cerr << "Error: Tried to delete directory! Aborting. " << std::endl;
        return false;
    }

    if(fs::remove(filename.c_str()) != 0 )
    {
        std::cerr << "Error deleting file: " << filename << std::endl;
        return false;
    }

    return true;
}

string FileSystemOperations::moveFile(string file, string new_dir)
{
    fs::path f(file);
    string basename = f.filename().string();
    fs::rename(file, new_dir + basename);

    return (new_dir + basename);
}

string FileSystemOperations::chopExtension(string path)
{
    size_t found;
    string key = ".";

	 found=path.rfind(key);
    if (found!=string::npos)
		  path.erase(found);
//    else
//        return "NotFound";

	 return path;
}

string FileSystemOperations::chopTrailingNumbers(string path)
{
	path = path.substr(0, path.find_last_not_of("0123456789")+1);
	return path;
}

std::string FileSystemOperations::getFolder(std::string path)
{
	path = path.substr(0, path.find_last_of("/")+1);
	return path;
}

std::string FileSystemOperations::getBaseName(std::string path)
{
	string::size_type division = path.find_last_of("/");
	if (division != string::npos)
		path = path.substr(division+1);
	path = chopExtension(path);

	return path;
}

string FileSystemOperations::getFileExtension(string path)
{
    size_t found;
    string key = ".";
    string ext;

	 found=path.rfind(key);
    if (found!=string::npos)
		  ext = path.substr(found+1);
    else
        return "NotFound";

    return ext;
}

int FileSystemOperations::parseFileNumber(string filename, string chop_before, string chop_after)
{
    size_t found;

    // Chop ending:
    found=filename.rfind(chop_after);
    if (found!=string::npos)
        filename.erase(found);
    else
        return -1;

    // Chop beginning:
    found=filename.rfind(chop_before);
    if (found == 0)
        filename.erase(0, chop_before.size());
    else
        return -1;

    // Parse rest:
    int ret_val = atoi(filename.c_str());

    return ret_val;
}

string FileSystemOperations::generateFileName(string basename, string extension)
{
    timeb tb;
    ftime(&tb);

    time_t timev = tb.time;
    struct tm *aTime = localtime(&timev);
    int day = aTime->tm_mday;
    int month = aTime->tm_mon + 1; // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
    int year = aTime->tm_year + 1900; // Year is # years since 1900

    stringstream retVal;
    retVal << basename
           << "_" << setfill('0') << setw(4) << year
           << setfill('0') << setw(2) << month
           << setfill('0') << setw(2) << day
           << "-" << setfill('0') << setw(2) << aTime->tm_hour
           << setfill('0') << setw(2) << aTime->tm_min
           << setfill('0') << setw(2) << aTime->tm_sec
           << "-" << setfill('0') << setw(3) << tb.millitm
           << "." << extension;

    return retVal.str();
}

string FileSystemOperations::generateFileNameByNumber(string path, string basename, string extension)
{
	vector<string> file_list;
	getFileList(path, file_list);

	string new_name = "";
	for (int num = 0; true; num++)
	{
		stringstream ss;
		ss << basename << num << "." << extension;
		string name_cand = ss.str();

		// Check if name already exists:
		bool name_exists = false;
		for (int file = 0; file < (int)file_list.size(); file++)
		{
			if (file_list[file] == name_cand)
			{
				name_exists = true;
				break;
			}
		}
		if (name_exists == false)
		{
			new_name = name_cand;
			break;
		}
	}

	return new_name;
}

string FileSystemOperations::generateFolderName(string basename)
{
    timeb tb;
    ftime(&tb);

    time_t timev = tb.time;
    struct tm *aTime = localtime(&timev);
    int day = aTime->tm_mday;
    int month = aTime->tm_mon + 1; // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
    int year = aTime->tm_year + 1900; // Year is # years since 1900

	stringstream retVal;
	retVal << basename;
	if (basename != "")
    {
		if (basename.at(basename.length()-1) != '/')
		{
			retVal << "_";
		}
    }
    retVal << setfill('0') << setw(4) << year << "-"
           << setfill('0') << setw(2) << month << "-"
           << setfill('0') << setw(2) << day << "_"
           << setfill('0') << setw(2) << aTime->tm_hour << "-"
           << setfill('0') << setw(2) << aTime->tm_min << "-"
           << setfill('0') << setw(2) << aTime->tm_sec << "-"
           << setfill('0') << setw(3) << tb.millitm << "/";

    return retVal.str();
}

bool FileSystemOperations::fileExists(string filename)
{
    return fs::exists(filename);
}


















