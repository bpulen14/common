#ifndef FILESYSTEMOPERATION_H_
#define FILESYSTEMOPERATION_H_

#define BOOST_FILESYSTEM_VERSION 3
#ifndef BOOST_FILESYSTEM_NO_DEPRECATED
#  define BOOST_FILESYSTEM_NO_DEPRECATED
#endif
#ifndef BOOST_SYSTEM_NO_DEPRECATED
#  define BOOST_SYSTEM_NO_DEPRECATED
#endif

#include <string>
#include <iostream>
#include <vector>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"
#include "boost/progress.hpp"

namespace DefaultLocations
{
	static const std::string SBS_DATA				= "/sbs_data/";
	static const std::string SBS_CAM_CALIBRATIONS= "/sbs_data/Calibrations/Cameras/";
	static const std::string SBS_WORKSTATIONS		= "/sbs_data/Calibrations/Workstations/";
	static const std::string SBS_IMGS				= "/sbs_data/SbsImgs/";
	static const std::string SBS_TASKS				= "/sbs_data/Tasks/";
	static const std::string SBS_WORLD_MODELS		= "/sbs_data/WorldModels/";
	static const std::string SBS_ROOT				= "/Skill_Based_System/";
	static const std::string SBS_ICONS				= "/Skill_Based_System/data/icons/";
//	static const std::string MOBILE_MANIPULATOR	= "/";
}

class FileSystemOperations{

public:

	FileSystemOperations(); //new task

	virtual ~FileSystemOperations();

	// Paths do not include trailing '/'
	static std::string getCurrentPath(void);
	static void getPath(std::string MobileManipulator_loc, char* out_path);
	static std::string getPath(std::string mobile_manipulator_loc);
	static std::string getAnyPath(std::string search_key, std::string path_from_key, bool replace_key);

	static bool getFileList(char path[200], std::vector<std::string>& task_list);
	static bool getFileList(std::string path_string, std::vector<std::string>& file_list);
	static bool getFileList(std::string path_string, std::vector<std::string>& file_list, std::string containing);

    static bool getFolderList(std::string path_string, std::vector<std::string>& folder_list);
	static bool getFolderList(std::string path_string, std::vector<std::string>& folder_list, std::string containing);

	static bool deleteFile(std::string path_string, std::string filename);
	static bool deleteFile(std::string filename);
	static std::string moveFile(std::string file, std::string new_dir);

	// Get parts of path/filename:
	static std::string getBaseName(std::string path);
	static std::string getFileExtension(std::string path);
	static std::string chopExtension(std::string path);
	static std::string chopTrailingNumbers(std::string path);
	static std::string getFolder(std::string path);

	static int parseFileNumber(std::string filename, std::string chop_before, std::string chop_after);
	static std::string generateFileName(std::string basename, std::string extension);
	static std::string generateFileNameByNumber(std::string path, std::string basename, std::string extension);
	static std::string generateFolderName(std::string basename);

	static bool fileExists(std::string filename);

	static void createDir(std::string dir);

private:
	struct FileTime
	{
		FileTime(std::string filename, std::time_t time) : filename_(filename), time_(time) {}
		std::string filename_;
		std::time_t time_;
	};

	struct by_time
	{
		bool operator()(FileTime const &ft1, FileTime const &ft2)
		{
			return ft1.time_ > ft2.time_;
		}
	};
};



#endif /* FILESYSTEMOPERATION_H_ */
